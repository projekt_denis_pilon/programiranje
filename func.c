#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "Header.h"

void unosNoveObuce(int* counter) {
	*counter = 0;
	FILE* fp = NULL;
	OBUCA* op = NULL;
	op = (OBUCA*)calloc(1, sizeof(OBUCA));
	if (op == NULL) {
		perror("Zauzimanje:");
		exit(EXIT_FAILURE);
	}
	fp = fopen("obuca.bin", "rb+");
	if (fp == NULL) {
		fp = fopen("obuca.bin", "wb+");
		if (fp == NULL) {
			perror("Otvaranje datoteke obuca:");
			exit(EXIT_FAILURE);
		}
	}
	fread(counter, sizeof(int), 1, fp);
	(*counter)++;
	rewind(fp);
	fwrite(counter, sizeof(int), 1, fp);
	printf("Unesite model obuce: ");
	getchar();
	fgets(op->model, 30, stdin);
	printf("Unesite marku obuce: ");
	fgets(op->marka, 30, stdin);
	printf("Unesite velicinu obuce: ");
	scanf("%f", &op->velicina);
	printf("Unesite cijenu obuce: ");
	scanf("%f", &op->cijena);
	fseek(fp, 0, SEEK_END);
	fwrite(op, sizeof(OBUCA), 1, fp);
	fclose(fp);
	free(op);
	return;
}

void unosNoveOdjece(int* counter) {
	*counter = 0;
	FILE* fp = NULL;
	ODJECA* op = NULL;
	op = (ODJECA*)calloc(1, sizeof(ODJECA));
	if (op == NULL) {
		perror("Zauzimanje:");
		exit(EXIT_FAILURE);
	}
	fp = fopen("odjeca.bin", "rb+");
	if (fp == NULL) {
		fp = fopen("odjeca.bin", "wb+");
		if (fp == NULL) {
			perror("Otvaranje datoteke obuca:");
			exit(EXIT_FAILURE);
		}
	}
	fread(counter, sizeof(int), 1, fp);
	(*counter)++;
	rewind(fp);
	fwrite(counter, sizeof(int), 1, fp);
	printf("Unesite model odjece: ");
	getchar();
	fgets(op->model, 30, stdin);
	printf("Unesite marku odjece: ");
	fgets(op->marka, 30, stdin);
	printf("Unesite velicinu odjece: ");
	scanf("%s", &op->velicina);
	printf("Unesite cijenu odjece: ");
	scanf("%f", &op->cijena);
	fseek(fp, 0, SEEK_END);
	fwrite(op, sizeof(ODJECA), 1, fp);
	fclose(fp);
	free(op);
	return;
}

void pretrazivanjeObuce() {
	//sortiranje uz pretrazivanje
	//rekurzivna funkcija (mozda) ili negdje drugdje
}

void pretrazicanjeOdjece() {

}

void prodanaObuca(int *counter) {
	int i;
	FILE* fp = NULL;
	OBUCA* traverseNode = NULL;
	OBUCA* headNode = NULL, *prodanaObuca=NULL, *head=NULL, *newHeadNode=NULL;
		fp = fopen("obuca.bin", "rb+");
		if (fp == NULL) {
			printf("\nSkladiste ne postoji, molimo kreirajte skladiste\n\n");
			return;
		}
		else {
			headNode = (OBUCA*)calloc(1, sizeof(OBUCA));
			if (headNode == NULL) {
				printf("Neuspjesno zauzimanje");
				exit(EXIT_FAILURE);
			}
			else {
				headNode->next = NULL;
				fread(headNode, sizeof(OBUCA), 1, fp);
				rewind(fp);
				fread(counter, sizeof(int), 1, fp);
				for (i = 0;i < (*counter);i++) {
					newHeadNode = (OBUCA*)calloc(1, sizeof(OBUCA));
					if (newHeadNode == NULL) {
						printf("Neuspjesno zauzimanje");
						exit(EXIT_FAILURE);
					}
					else {
						fread(newHeadNode, sizeof(OBUCA), 1, fp);
						newHeadNode->next = headNode;
						headNode = newHeadNode;

					}
				}
				prodanaObuca = (OBUCA*)calloc(1, sizeof(OBUCA));
				if (prodanaObuca == NULL) {
					printf("Neuspjesno zauzimanje");
					exit(EXIT_FAILURE);
				}
				else {
					printf("Unesite podatke o prodanoj obuci:\nMarka: ");
					getchar();
					fgets(prodanaObuca->marka, 30, stdin);
					printf("Velicina: ");
					scanf("%f", &prodanaObuca->velicina);
					traverseNode = NULL;
					OBUCA* targetedNode = headNode;
					while (traverseNode != NULL) {
						if (strcmp(prodanaObuca->marka, traverseNode->marka)==0) {
							if (prodanaObuca->velicina == traverseNode->velicina) {
								targetedNode = traverseNode;
							}
						}
						traverseNode = traverseNode->next;
					}
					if (headNode == targetedNode) {
						headNode = headNode->next;
						free(targetedNode);
					}
					else {
						traverseNode = headNode;
						while (traverseNode->next != 0) {
							if (traverseNode->next == targetedNode) {
								traverseNode->next = targetedNode->next;
								free(targetedNode);
							}
							traverseNode = traverseNode->next;
						}
					}
				}
			}
			fclose(fp);
			(*counter)--;
			fp=fopen("obuca", "wb");
			if (fp == NULL) {
				perror("Otvaranje nove datoteke:");
				exit(EXIT_FAILURE);
			}
			fwrite(counter, sizeof(int), 1, fp);
			traverseNode = headNode;
			for (i = 0;i < *counter;i++) {
				fwrite(traverseNode, sizeof(OBUCA), 1, fp);
				traverseNode = traverseNode->next;
			}
			OBUCA* deleteNode = NULL;
			while (traverseNode != NULL) {
				deleteNode = traverseNode;
				traverseNode = traverseNode->next;
				free(deleteNode);
			}
			fclose(fp);
			system("cls");
			printf("\nObuca uspjesno obrisana iz skladi�ta\n\n");
			
		}
		return;
}


void prodanaOdjeca(int *counter) {
	//povezani popis
}

void ispisObuce(int * counter) {
	int i=0;
	FILE* fp = NULL;
	OBUCA* op = NULL;
	fp = fopen("obuca.bin", "rb+");
	if (fp == NULL) {
		system("cls");
		printf("\nSkladiste nije kreirano, molimo kreirajte skladiste.\n\n");
		return;
	}
	else {
		rewind(fp);
		fread(counter, sizeof(int), 1, fp);
		op = (OBUCA*)calloc(*counter, sizeof(OBUCA));
		if (op == NULL) {
			printf("Gre�ka prilikom zauzimanja memorijE");
			exit(EXIT_FAILURE);
		}
		fread(op, sizeof(OBUCA), *counter, fp);
		system("cls");
		for (i = 0;i < (*counter);i++) {
			printf("\nModel: %s", (op + i)->model);
			printf("Marka: %s", (op + i)->marka);
			printf("Velicina: %.2f\n", (op + i)->velicina);
			printf("Cijena: %.2f\n\n", (op + i)->cijena);
		}
		fclose(fp);
		free(op);
	}
	return;
}

void ispisOdjece(int *counter){
	int i = 0;
	FILE* fp = NULL;
	ODJECA* op = NULL;
	fp = fopen("odjeca.bin", "rb+");
	if (fp == NULL) {
		system("cls");
		printf("\nSkladiste nije kreirano, molimo kreirajte skladiste.\n\n");
		return;
	}
	else {
		rewind(fp);
		fread(counter, sizeof(int), 1, fp);
		op = (ODJECA*)calloc(*counter, sizeof(ODJECA));
		if (op == NULL) {
			printf("Gre�ka prilikom zauzimanja memorije");
			exit(EXIT_FAILURE);
		}
		fread(op, sizeof(ODJECA), *counter, fp);
		system("cls");
		for (i = 0;i < (*counter);i++) {
			printf("\nModel: %s", (op + i)->model);
			printf("Marka: %s", (op + i)->marka);
			printf("Velicina: %s\n", (op + i)->velicina);
			printf("Cijena: %.2f\n\n", (op + i)->cijena);
		}
		fclose(fp);
		free(op);
	}
	return;
}