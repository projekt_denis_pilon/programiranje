#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "Header.h"

//system("cls");

int main() {

	int n = 0, counter = 0;
	while (n != 9) {
		printf("/*****************************************\n");
		printf("Unesite broj radnje koju zelite izvrsiti:\n");
		printf("1. Unos nove obuce.\n");
		printf("2. Unos nove odjece.\n");
		printf("3. Pretazivanje obuce.\n");
		printf("4. Pretrazvanje odjece.\n");
		printf("5. Prodana obuca.\n");
		printf("6. Prodana odjeca.\n");
		printf("7. Ispis cijelog skladista obuce.\n");
		printf("8. Ispis cijelog skladista odjece.\n");
		printf("9. Exit.\n");
		printf("*****************************************/\n");
		do {
			scanf("%d", &n);
			if (n<1 || n>9) {
				printf("Krivo unesena vrijednost, ponovite unos:\n");
			}
		} while (n < 1 || n>9);
		switch (n) {
		case 1:
			unosNoveObuce(&counter);
			system("cls");
			printf("\nObuca uspjesno unesena.\n\n");
			break;
		case 2:
			unosNoveOdjece(&counter);
			system("cls");
			printf("\nOdjeca uspjesno unesena.\n\n");
			break;
		case 3:
			pretrazivanjeObuce();
			break;
		case 4:
			pretrazicanjeOdjece();
			break;
		case 5:
			prodanaObuca(&counter);
			break;
		case 6:
			prodanaOdjeca(&counter);
			break;
		case 7:
			ispisObuce(&counter);
			break;
		case 8:
			ispisOdjece(&counter);	
			break;
		default:
			break;
		}


	}
	return 0;
}